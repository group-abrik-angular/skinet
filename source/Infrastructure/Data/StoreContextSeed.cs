﻿using Core.Entities;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Infrastructure.Data
{
    public class StoreContextSeed
    {
        public static async Task SeedAsync(StoreContext context, ILoggerFactory loggerFactory)
        {
            try
            {
                if (!context.ProductBrands.Any())
                {
                    var data = File.ReadAllText("../Infrastructure/Data/SeedData/brands.json");
                    var items = JsonSerializer.Deserialize<List<ProductBrand>>(data);

                    foreach (var item in items)
                    {
                        context.ProductBrands.Add(item);
                    }
                    await context.SaveChangesAsync();
                }

                if (!context.ProductTypes.Any())
                {
                    var data = File.ReadAllText("../Infrastructure/Data/SeedData/types.json");
                    var items = JsonSerializer.Deserialize<List<ProductType>>(data);

                    foreach (var item in items)
                    {
                        context.ProductTypes.Add(item);
                    }
                    await context.SaveChangesAsync();
                }

                if (!context.Products.Any())
                {
                    var data = File.ReadAllText("../Infrastructure/Data/SeedData/products.json");
                    var items = JsonSerializer.Deserialize<List<Product>>(data);

                    foreach (var item in items)
                    {
                        context.Products.Add(item);
                    }
                    await context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                var log = loggerFactory.CreateLogger<StoreContextSeed>();
                log.LogError(ex.Message);
            }
        }

    }
}
