﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Specifications
{
    public class ProductWithFiltersFromCountSpecification: BaseSpecification<Product>
    {
        public ProductWithFiltersFromCountSpecification(ProductSpecParams par)
            : base(x =>
                (string.IsNullOrEmpty(par.Search) || x.Name.ToLower().Contains(par.Search)) &&
                 (!par.BrandId.HasValue || x.ProductBrandId == par.BrandId) &&
                 (!par.TypeId.HasValue || x.ProductTypeId == par.TypeId)
                )
        {

        }
    }
}
